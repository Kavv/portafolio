import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LandingRoutingModule } from './landing-routing.module';
import { NgbCarousel, NgbCarouselConfig, NgbCarouselModule, NgbModal, NgbModalModule, NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { MainComponent } from './main/main.component';


@NgModule({
  declarations: [
    MainComponent
  ],
  imports: [
    CommonModule,
    LandingRoutingModule,
    NgbModule,
    NgbCarouselModule,
    NgbModalModule
  ]
})
export class LandingModule { }
