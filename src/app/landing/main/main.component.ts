import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { NgbCarouselConfig, NgbCarousel, NgbModal, NgbModule } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss'],
  providers: [NgbCarouselConfig] 
})

export class MainComponent implements OnInit {

  showNavigationArrows = false;
  showNavigationIndicators = false;
  images = [1055, 194, 368].map((n) => `https://picsum.photos/id/${n}/900/500`);
  baseImg = 'assets/img/';
  imgModal: any;
  imgRes = ['assets/img/fatsaas-r1.PNG', 'assets/img/fatsaas-r2.PNG', 'assets/img/fatsaas-r3.PNG'];
  imgWeb = ['assets/img/fm.PNG', 'assets/img/fm2.PNG', 'assets/img/fm3.PNG'];

  imgTitle: any;
  resTitles = ['Customer Infomation', 'Services List', 'Preview Invoice'];
  webTitles = ['Editable Multimedia', 'Change | Delete Images', 'Enterprice Landing'];
  
  imgCS = ['assets/img/cs-reservation.PNG', 'assets/img/cs-reservation2.PNG', 'assets/img/cs-reservation2.1.PNG',
           'assets/img/cs-reservation3.PNG', 'assets/img/cs-reservation4.PNG', 'assets/img/cs-reservation5.PNG'];
  csTitles = ['Home', 'Office Hours Reservation', 'Assign attendance to students', 
              'Sumary with Datatables', 'Sumary with HighCharts' , 'Sumary with HighCharts'];

  landing = "";

  constructor(config: NgbCarouselConfig, private modalService: NgbModal) {
    // customize default values of carousels used by this component tree
    config.showNavigationArrows = true;
    config.showNavigationIndicators = true;
  }
  
  openLg(content:any, option: any) {
    this.landing = "";
    if(option == 1)
    {
      this.imgTitle = this.resTitles;
      this.imgModal = this.imgRes;
    }
    if(option == 2)
    {
      this.imgTitle = this.webTitles;
      this.imgModal = this.imgWeb;
      this.landing = "http://alquileryeventosantana.com/";
    }
    if(option == 3)
    {
      this.imgTitle = this.csTitles;
      this.imgModal = this.imgCS;
    }
    this.modalService.open(content, { size: 'lg' });
  }

  openXl(content:any) {
    this.modalService.open(content, { size: 'xl' });
  }

  openVerticallyCentered(content:any) {
    this.modalService.open(content, { centered: true });
  }

  @ViewChild("carousel") carousel:any = NgbCarousel;
  change_step(id:any) {
    this.carousel.select(id);
  }

  ngOnInit(): void {
  }

}
