// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  firebase: {
    projectId: 'portafolio-97715',
    appId: '1:700103588319:web:04d4bcbd56b04629462c6a',
    storageBucket: 'portafolio-97715.appspot.com',
    apiKey: 'AIzaSyB_a4D8RIm9c_suYvWmqQ7jsEdy-a8uWEI',
    authDomain: 'portafolio-97715.firebaseapp.com',
    messagingSenderId: '700103588319',
    measurementId: 'G-3SLXQ61W22',
  },
  production: false
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
